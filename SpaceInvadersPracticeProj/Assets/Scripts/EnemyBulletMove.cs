﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletMove : MonoBehaviour
{
    public const int LATE_SHOT_TIME = 22;
    public const int EARLY_SHOT_TIME = 2;

    public GameObject enemyBullet;

    private float nextShotTime = 0.0f;

    public static System.Random randomSeed = new System.Random();

    // Start is called before the first frame update
    void Start()
    {
        nextShotTime = GenerateNewShotTime();
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextShotTime)
        {
            // Shoot a bullet down.
            GameObject bullet = Instantiate(enemyBullet, transform.position, Quaternion.identity);

            // Ignore collisions with all enemies.
            Transform spawner = transform.parent.parent;
            for(int iArmy = 0; iArmy < spawner.childCount; iArmy++)
            {
                for (int iEnemy = 0; iEnemy < spawner.GetChild(iArmy).childCount; iEnemy++)
                {
                    Physics.IgnoreCollision(bullet.GetComponent<Collider>(), spawner.GetChild(iArmy).GetChild(iEnemy).GetComponent<Collider>());
                }
            }

            // Find a new shot time.
            nextShotTime = GenerateNewShotTime();
        }
    }

    // Returns new time to shoot a bullet at the player.
    private float GenerateNewShotTime()
    {
        float newShotTime = 0.0f;

        newShotTime = randomSeed.Next((int)(Time.time + EARLY_SHOT_TIME), (int)(Time.time + LATE_SHOT_TIME));

        return newShotTime;
    }
}
