﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollideWithPlayer : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag.Equals("Player"))
        {
            if(PlayerStats.lives >= 0)
            {
                PlayerStats.lives--;
            }  
        }

        Destroy(gameObject);
    }

}
