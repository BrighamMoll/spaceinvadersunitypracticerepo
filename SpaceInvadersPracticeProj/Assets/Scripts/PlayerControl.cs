﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]

public class PlayerControl : MonoBehaviour
{
    public GameObject playerBuletPref;

    private Rigidbody rb;
    private const float speed = 5f;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(-moveHorizontal, 0f, 0f);

        if (Input.GetKey("left"))
        {
            transform.position += movement * speed * Time.deltaTime;
        }

        if (Input.GetKey("right"))
        {
            transform.position += new Vector3(-moveHorizontal, 0f, 0f) * speed * Time.deltaTime;
        }
    }

    private void Update()
    {
        PlayerBullet.nextShootTimer -= Time.deltaTime;
        if (PlayerBullet.nextShootTimer <= 0)
        {
            PlayerBullet.canShoot = true;
            PlayerBullet.nextShootTimer = 1.5f;
        }
        
        if (Input.GetKeyDown("space"))
        {
            if (PlayerBullet.canShoot == true)
            {
                Shoot();
                PlayerBullet.canShoot = false;
            }
        }

        if (PlayerStats.lives == 0)
        {
            Destroy(gameObject);
        }
    }

    private void Shoot()
    {
        Vector3 bulletPos = this.transform.position + new Vector3(0f, 0, -1f);

        GameObject bulletGO = (GameObject)Instantiate(playerBuletPref, bulletPos, Quaternion.identity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "LeftWall")
        {
            Vector3 newPos = new Vector3(0.2f, 0f, 0f);
            transform.position -= newPos;
        }

        if (collision.collider.tag == "RightWall")
        {
            Vector3 newPos = new Vector3(0.2f, 0f, 0f);
            transform.position += newPos;
        }
    }

}
