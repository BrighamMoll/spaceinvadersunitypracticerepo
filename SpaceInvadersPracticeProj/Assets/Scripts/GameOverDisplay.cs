﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverDisplay : MonoBehaviour
{

    public Text finalScore;
    

    private void Update()
    {
        finalScore.text = PlayerStats.score.ToString();
    }

    public void Retry()
    {
        PlayerStats.lives = 3;
        PlayerStats.score = 0;
        SceneManager.LoadScene("Test");
    }

    public void Quit()
    {
        Application.Quit();
    }

}
