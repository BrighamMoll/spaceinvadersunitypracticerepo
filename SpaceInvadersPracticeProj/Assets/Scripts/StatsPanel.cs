﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsPanel : MonoBehaviour
{
    public Text score;
    public Text lives;

    private void Update()
    {
        score.text = PlayerStats.score.ToString();
        lives.text = PlayerStats.lives.ToString();
    }
}
