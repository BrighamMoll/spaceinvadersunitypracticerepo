﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletMomentum : MonoBehaviour
{
    public const float ENEMY_BULLET_SPEED = 0.05f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(0f, 0f, ENEMY_BULLET_SPEED);
    }

    // Despawn bullet upon hitting death wall.
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "The Death Wall")
        {
            Destroy(gameObject);
        }
    }
}
