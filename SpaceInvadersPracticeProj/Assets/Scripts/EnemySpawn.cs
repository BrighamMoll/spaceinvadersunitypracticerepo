﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public const float SPAWN_INTERVAL = 30.0f;

    private float nextSpawnTime = 0.0f;

    public GameObject basicArmy;

    // Start is called before the first frame update
    void Start()
    {
        nextSpawnTime = Time.time + SPAWN_INTERVAL;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > nextSpawnTime)
        {
            GameObject newArmy = Instantiate(basicArmy, transform.position, Quaternion.identity);

            newArmy.transform.parent = transform;

            nextSpawnTime = Time.time + SPAWN_INTERVAL;
        }
    }
}
