﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject statsPanel;
    public GameObject gameOverPanel;

    private void Update()
    {
        if (PlayerStats.lives == 0)
        {
            statsPanel.SetActive(false);
            gameOverPanel.SetActive(true);
        }
            
    }

}
