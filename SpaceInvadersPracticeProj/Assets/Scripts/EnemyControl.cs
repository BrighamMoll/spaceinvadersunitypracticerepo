﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyControl : MonoBehaviour
{
    public const float ENEMY_SPEED = 0.01f;
    public const float SWITCH_TIME = 2.5f;
    public const float DOWN_MULTIPLIER = 50.0f;

    private float nextSwitchTime = 0.0f;

    private bool movingLeft = false;

    // Start is called before the first frame update
    void Start()
    {
        nextSwitchTime = Time.time + SWITCH_TIME;
    }

    // Update is called once per frame
    void Update()
    {
        if(movingLeft)
        {
            transform.position += new Vector3(ENEMY_SPEED, 0f, 0f);
        }
        else
        {
            transform.position += new Vector3(-ENEMY_SPEED, 0f, 0f);
        }

        if(Time.time  > nextSwitchTime)
        {
            movingLeft = !movingLeft;

            transform.position += new Vector3(0f, 0f, DOWN_MULTIPLIER*ENEMY_SPEED);

            nextSwitchTime += SWITCH_TIME;
        }
    }

    // Despawn enemy and decrease lives upon hitting death wall.
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "The Death Wall")
        {
            // Destroy enemy object.
            Destroy(gameObject);
            PlayerStats.lives--;
        }
    }
}
