﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletMove : MonoBehaviour
{
    public float playerBulletSpeed = 10f;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(0f, 0f, -4f);
        transform.position += movement * Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "enemy")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
            PlayerBullet.canShoot = true;
            PlayerStats.score += 500;
        }

        if(collision.collider.tag == "TopWall")
        {
            Destroy(gameObject);
            PlayerBullet.canShoot = true;
        }
    }
}
